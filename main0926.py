

# main function

# Import system modules
import arcpy
from dbfread import DBF
import csv
import numpy as np
import pandas as pd
import collections
def main():
    print("Hello World!")

    # Set the workspace
    arcpy.env.workspace = 'C:/Data/soil/soildb.gdb'
    arcpy.env.cellSize = 'C:/Data/soil/soildb.gdb/MapunitRaster_10m'
    arcpy.env.overwriteOutput = True
    arcpy.CheckOutExtension("Spatial")

    fc = "C:/Data/soil/soildb.gdb/buffer5mi"
    field = "code"
    bufferIDs = []
    cursor = arcpy.SearchCursor(fc)
    for row in cursor:
        print(row.getValue(field))
        bufferIDs.append(row.getValue(field))
    print(len(bufferIDs))
    # Select all cities which overlap the chihuahua polygon
    # chihuahua_cities = arcpy.SelectLayerByLocation_management('cities', 'INTERSECT','chihuahua', 0,'NEW_SELECTION')

    # make a feature layer
    arcpy.MakeFeatureLayer_management("C:/Data/soil/soildb.gdb/buffer5mi", "bufferlayer")

    for i in bufferIDs:
        # farmBuffer = arcpy.SelectLayerByAttribute_management("bufferlayer", "NEW_SELECTION",'"Sample" = 1')
        farmBuffer = arcpy.SelectLayerByAttribute_management("bufferlayer", "NEW_SELECTION",'"code" = ' + str(i))

        strBufferName = "C:/Data/soil/buffer5mi.gdb/" + "buf5m" +str(i)
        # Write the selected features to a new featureclass
        arcpy.CopyFeatures_management(farmBuffer, strBufferName)

        # Set local variables
        inZoneData = strBufferName
        zoneField = "code"
        inClassData = "MapunitRaster_10m"
        classField = "MUKEY"
        outTable = "output5mi/" + str(i) + ".dbf"
        # outTable = "test/" + str(i) + ".dbf"
        # processingCellSize = 2

        # Check out the ArcGIS Spatial Analyst extension license

        # Execute TabulateArea
        arcpy.sa.TabulateArea(inZoneData, zoneField, inClassData, classField, outTable)
def test():
    # Creating an empty Dataframe with column names only
    # dfObj = pd.DataFrame(columns=['User_ID', 'UserName', 'Action'])
    #
    # print("Empty Dataframe ", dfObj)
    #
    # print('*** Appends rows to an empty DataFrame using dictionary with default index***')
    #
    # # Append rows in Empty Dataframe by adding dictionaries
    # dfObj = dfObj.append({'User_ID': 23, 'Action': 'Login','UserName': 'Riti'}, ignore_index=True)
    # dfObj = dfObj.append({'User_ID': 24, 'UserName': 'Aadi', 'Action': 'Logout'}, ignore_index=True)
    # dfObj = dfObj.append({'User_ID': 25, 'UserName': 'Jack', 'Action': 'Login'}, ignore_index=True)
    #
    #
    # print("Dataframe Contens ", dfObj)

    allcols = ['mukey', 'slope', 'slopelenusle', 'tfact', 'wei', 'elev', 'ffd', 'fraggt10', 'frag3to10', 'sieveno4', 'sieveno10',
     'sieveno40', 'sandtotal', 'silttotal', 'claytotal', 'om', 'bulkDensity', 'ksat', 'awc', 'lep', 'kwfact', 'kffact',
     'caco3', 'gypsum', 'sar', 'ec', 'cec7', 'ecec', 'sumbases', 'ph1to1h2o', 'ph01mcacl2', 'muname', 'compname',
     'compkind', 'majcompflag', 'runoff', 'weg', 'erocl', 'earthcovkind1', 'earthcovkind2', 'hydricon', 'hydricrating',
     'drainagecl', 'nirrcapcl', 'nirrcapscl', 'nirrcapunit', 'irrcapcl', 'irrcapscl', 'irrcapunit', 'hydgrp',
     'taxorder', 'taxsuborder', 'taxgrtgroup', 'taxsubgrp', 'taxpartsize', 'taxpartsizemod', 'taxceactcl',
     'taxreaction', 'taxtempcl', 'taxmoistscl', 'taxtempregime', 'soiltaxedition', 'cokey', 'texcl', 'max_comppct',
     'aws0_5', 'aws5_20', 'aws20_50', 'aws50_100', 'aws100_150', 'aws150_999', 'aws0_20', 'aws0_30', 'aws0_100',
     'aws0_150', 'aws0_999', 'tk0_5a', 'tk5_20a', 'tk20_50a', 'tk50_100a', 'tk100_150a', 'tk150_999a', 'tk0_20a',
     'tk0_30a', 'tk0_100a', 'tk0_150a', 'tk0_999a', 'musumcpcta', 'soc0_5', 'soc5_20', 'soc20_50', 'soc50_100',
     'soc100_150', 'soc150_999', 'soc0_20', 'soc0_30', 'soc0_100', 'soc0_150', 'soc0_999', 'tk0_5s', 'tk5_20s',
     'tk20_50s', 'tk50_100s', 'tk100_150s', 'tk150_999s', 'tk0_20s', 'tk0_30s', 'tk0_100s', 'tk0_150s', 'tk0_999s',
     'musumcpcts', 'nccpi3corn', 'nccpi3soy', 'nccpi3cot', 'nccpi3sg', 'nccpi3all', 'pctearthmc', 'rootznemc',
     'rootznaws', 'droughty', 'pwsl1pomu', 'musumcpct']
    dtypes = {}
    for c in allcols:
        if c == 'mukey':
            dtypes[c] = 'str'
        else:
            dtypes[c] = 'float64'

    soildf = pd.read_csv('C:/data/soil/mapunit_soils_data_new.csv', usecols=allcols, dtype = dtypes)
    print(soildf.head())
    print(soildf.dtypes)
    print(list(soildf))

def processData():
    print("process data")
    fc = "C:/Data/soil/soildb.gdb/buffer1mi"
    field = "code"
    bufferIDs = []
    cursor = arcpy.SearchCursor(fc)
    for row in cursor:
        print(row.getValue(field))
        bufferIDs.append(row.getValue(field))
    print(len(bufferIDs))

    # for record in DBF('C:/Data/soil/output1mi/5009.dbf'):
    #     print(record)

    # read the soil attribute data from the csv

    allcols = ['mukey', 'slope', 'slopelenusle', 'tfact', 'wei', 'elev', 'ffd', 'fraggt10', 'frag3to10', 'sieveno4', 'sieveno10',
     'sieveno40', 'sandtotal', 'silttotal', 'claytotal', 'om', 'bulkDensity', 'ksat', 'awc', 'lep', 'kwfact', 'kffact',
     'caco3', 'gypsum', 'sar', 'ec', 'cec7', 'ecec', 'sumbases', 'ph1to1h2o', 'ph01mcacl2', 'muname', 'compname',
     'compkind', 'majcompflag', 'runoff', 'weg', 'erocl', 'earthcovkind1', 'earthcovkind2', 'hydricon', 'hydricrating',
     'drainagecl', 'nirrcapcl', 'nirrcapscl', 'nirrcapunit', 'irrcapcl', 'irrcapscl', 'irrcapunit', 'hydgrp',
     'taxorder', 'taxsuborder', 'taxgrtgroup', 'taxsubgrp', 'taxpartsize', 'taxpartsizemod', 'taxceactcl',
     'taxreaction', 'taxtempcl', 'taxmoistscl', 'taxtempregime', 'soiltaxedition', 'cokey', 'texcl', 'max_comppct',
     'aws0_5', 'aws5_20', 'aws20_50', 'aws50_100', 'aws100_150', 'aws150_999', 'aws0_20', 'aws0_30', 'aws0_100',
     'aws0_150', 'aws0_999', 'tk0_5a', 'tk5_20a', 'tk20_50a', 'tk50_100a', 'tk100_150a', 'tk150_999a', 'tk0_20a',
     'tk0_30a', 'tk0_100a', 'tk0_150a', 'tk0_999a', 'musumcpcta', 'soc0_5', 'soc5_20', 'soc20_50', 'soc50_100',
     'soc100_150', 'soc150_999', 'soc0_20', 'soc0_30', 'soc0_100', 'soc0_150', 'soc0_999', 'tk0_5s', 'tk5_20s',
     'tk20_50s', 'tk50_100s', 'tk100_150s', 'tk150_999s', 'tk0_20s', 'tk0_30s', 'tk0_100s', 'tk0_150s', 'tk0_999s',
     'musumcpcts', 'nccpi3corn', 'nccpi3soy', 'nccpi3cot', 'nccpi3sg', 'nccpi3all', 'pctearthmc', 'rootznemc',
     'rootznaws', 'droughty', 'pwsl1pomu', 'musumcpct']


    dtypes = {'mukey':'str','soc0_150':'float64','droughty':'float64','nirrcapcl1':'float64','nirrcapcl2':'float64','nirrcapcl3':'float64',
              'nirrcapcl4':'float64', 'nirrcapcl5':'float64', 'nirrcapcl6':'float64', 'nirrcapcl7':'float64', 'nirrcapcl8':'float64',
             'Clay':'float64', 'Loam':'float64', 'Sand':'float64', 'ph_less6':'float64', 'ph_greater7dot5':'float64'
    }

    cols = ['mukey','soc0_150', 'droughty','nirrcapcl1','nirrcapcl2','nirrcapcl3', 'nirrcapcl4','nirrcapcl5','nirrcapcl6',
            'nirrcapcl7','nirrcapcl8','Clay','Loam','Sand','ph_less6','ph_greater7dot5']

    test_cols = ['soc0_150', 'droughty','nirrcapcl1','nirrcapcl2','nirrcapcl3', 'nirrcapcl4','nirrcapcl5','nirrcapcl6',
            'nirrcapcl7','nirrcapcl8','Clay','Loam','Sand','ph_less6','ph_greater7dot5']

    rcols = ['id','soc0_150', 'droughty','nirrcapcl1','nirrcapcl2','nirrcapcl3', 'nirrcapcl4','nirrcapcl5','nirrcapcl6',
            'nirrcapcl7','nirrcapcl8','Clay','Loam','Sand','ph_less6','ph_greater7dot5']

    rdf = pd.DataFrame(columns=rcols)
    # print(rdf)
    # test = {'slope': 1.5221, 'slopelenusle': 33.0386, 'wei': 47.4414, 'elev': 479.1314, 'tfact': 3.8901, 'id': '5041'}
    # rdf = rdf.append(test, ignore_index=True)
    # print(rdf)

    soildf = pd.read_csv('C:/data/soil/mapunit_soils_data_new.csv', usecols=cols, dtype = dtypes)
    # soildf = pd.read_csv('C:/data/soil/mapunit_soils_data.csv')
    # print(soildf.head())
    # print(soildf.dtypes)


    # bufferIDs = ['5054','5056','5041']

    # iterate the dbf files
    for i in bufferIDs:
        # 1-mile buffer
        # strDBF =  'C:/Data/soil/output1mi/' + str(i) + '.dbf'
        # 3-mile buffer
        # strDBF =  'C:/Data/soil/output3mi/' + str(i) + '.dbf'

        # 5-mile buffer
        strDBF =  'C:/Data/soil/output1mi/' + str(i) + '.dbf'

        table = DBF(strDBF, load=True)
        # table = DBF('C:/Data/soil/output1mi/5009.dbf')
        # print(len(table))
        print("--------------------")
        # print(table.records[0].items())

        # bufpd = pd.DataFrame(iter(table))
        # print(bufpd)

        oldDict = table.records[0].items()
        newDict = {}
        for key,value in oldDict:
            if key =='CODE':
                continue;
            else:
                # print(key,value)
                strKeys = key.split('_')
                if (len(strKeys) == 2):
                    # print(strKeys[1])
                    soilID = strKeys[1]
                    newDict[soilID] = float(value)/100

        # print(newDict)

        bufferdf = pd.DataFrame(list(newDict.items()), columns=['mukey','area'])
        # print(bufferdf)

        merge_df = pd.merge(left=bufferdf,right=soildf, how='left', left_on='mukey', right_on='mukey')
        # print(merge_df)
        # do not fill the null values
        # df = merge_df.fillna(0)
        df = merge_df
        # print(df)
        buf_dict = {}
        buf_dict['id'] = i

        # sum_area = df['area'].sum()
        for c in test_cols:
            # creating bool series True for NaN values
            bool_series = pd.notnull(df[c])
            tempdf = df[bool_series]
            # df[c] = df[c] * df['area']/sum_area
            # print(tempdf)
            weight_mean = (df['area'] * df[c]).sum() / df['area'].sum()
            weight_mean = round(weight_mean, 4)
            buf_dict[c] = weight_mean

        print(buf_dict)

        rdf = rdf.append(buf_dict, ignore_index=True)


    print(rdf)
    rdf.to_csv("C:/data/soil/1mi_result_new.csv")

if __name__ == "__main__":
    # main()
    processData()
    # test()
